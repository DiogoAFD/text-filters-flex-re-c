#include "estrutura.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>


Authors authors;
CoAuthors coAuthors;


CoAuthors initCoAuthors(){
	int i;
	CoAuthors c = (CoAuthors)malloc(sizeof(struct coAuthors));
	c->names = (char**)malloc(ARRAY_SIZE*sizeof(char*));
	for(i=0;i<ARRAY_SIZE;i++){
		c->names[i] = NULL;
	}
	c->size = ARRAY_SIZE;
	c->length = 0;
	return c;
}

Authors newNode (char *name){
	Authors node = (Authors)malloc(sizeof(author_node));
	node->name = name;
	node->coAuthors = initCoAuthors(); 
	node->left = NULL;
	node->right = NULL;
	node->height = 1;
	return node;
}

void addCoAuthorToAVL(CoAuthors coAuthors, char *coAuthor){
	int i,resize;
	if(ARRAY_SIZE == coAuthors->length){
		resize = (coAuthors->size)*2;
		coAuthors->names = realloc(coAuthors->names, resize);
		coAuthors->size = resize;
		for(i=coAuthors->length; i < resize; i++) 
			coAuthors->names[i] = NULL;
	}
	coAuthors->names[coAuthors->length] = strdup(coAuthor);
	coAuthors->length++;
}

Authors rightRotate(Authors y){

    Authors x = y->left;
    Authors z = x->right;
 
    x->right = y;
    y->left = z;
 
    y->height = max(height(y->left), height(y->right))+1;
    x->height = max(height(x->left), height(x->right))+1;
 
    return x;
}
 
Authors leftRotate(Authors x){

    Authors y = x->right;
    Authors T2 = y->left;
 
    y->left = x;
    x->right = T2;
 
    x->height = max(height(x->left), height(x->right))+1;
    y->height = max(height(y->left), height(y->right))+1;

    return y;
}

Authors insert(Authors node, Authors new){
	int balance = 0;
	if (node == NULL)
        return new;
	if (strcmp(new->name,node->name) != 0){
		if (strcmp(new->name,node->name) < 0) 
			node->left  = insert(node->left, new);
        else node->right = insert(node->right, new);

    	node->height = max(height(node->left), height(node->right)) + 1;
		balance = getBalance(node);

		if (balance > 1 && strcmp(new->name,node->left->name) < 0) 
			return rightRotate(node);
		if (balance < -1 &&  strcmp(new->name,node->right->name) >= 0) 
			return leftRotate(node);
        if (balance > 1 &&  strcmp(new->name,node->left->name) >= 0){
            node->left =  leftRotate(node->left);
            return rightRotate(node);
        }
        if (balance < -1 &&  strcmp(new->name,node->right->name) < 0){
            node->right = rightRotate(node->right);
            return leftRotate(node);
        }
     	return node;
    }
    else {return node;}
 }

int search(Authors node, char* name){

	if(node == NULL) return 0;
	if (strcmp(name,node->name) == 0) return 1;
    else if (strcmp(name,node->name) < 0) 
    	return search(node->left, name);
    else return search(node->right, name);
	
}

Authors getNodeByName(Authors node, char *name){
	if(node == NULL) return 0;
	if (strcmp(name,node->name) == 0) return node;
    else if (strcmp(name,node->name) < 0) 
    	return getNodeByName(node->left, name);
    else return getNodeByName(node->right, name);
}

void addAuthors(){
int i = 0,j,res;
char *name;
Authors new;

while(i < coAuthors->length){
	name = coAuthors->names[i];
	if(!search(authors,name)){
		new = newNode(name);
		for(j = 0; j < coAuthors->length; j++){
		  if(j != i) 
		   addCoAuthorToAVL(new->coAuthors,coAuthors->names[j]);
	}
	authors = insert(authors,new);

	}else{
	 new = getNodeByName(authors,name);
	 for(j=0; j < coAuthors->length; j++){
	  if(j != i) 
		addCoAuthorToAVL(new->coAuthors,coAuthors->names[j]);
     }
	}
	i++;
}
}


int coAuthorExists(char* coAuthor){
	int i;
	for(i=0; i < coAuthors->length; i++)
		if(!strcmp(coAuthors->names[i], coAuthor)) return 1;
	return 0;
}

void addCoAuthor(char *coAuthor){
	int i,resize;
	if(!coAuthorExists(coAuthor)){
		if(ARRAY_SIZE == coAuthors->length){
			resize = (coAuthors->size)*2;
			coAuthors->names = realloc(coAuthors->names, resize);
			coAuthors->size = resize;
			for(i=coAuthors->length; i < resize; i++) 
				coAuthors->names[i] = NULL;
		}
		coAuthors->names[coAuthors->length] = strdup(coAuthor);
		coAuthors->length++;
	}

}

int height(Authors A){
	if (A == NULL) return 0;
	return A-> height;
}

int max(int a, int b){
	return (a > b) ? a : b;
}

int getBalance(Authors A){
    if (A == NULL)
        return 0;
    return height(A->left) - height(A->right);
}

void init(){
	coAuthors = initCoAuthors();
}

void createGraph(Authors author){
	FILE* file;

	char *fileName = "graph.dot";

	file=fopen(fileName,"w+");

	fprintf(file, "digraph coAuthors {\n");
	
	int i = 0;
	int len = author->coAuthors->length;

	fprintf(file, "\t\"%s\" [shape=box];\n",author->name);

	if(len==0){
		fprintf(file, "\t\"%s\"\n", author->name);
	}else{
		while(i < len){
			fprintf(file, 
				"\t\"%s\" -> \"%s\"\n",
				author->name, author->coAuthors->names[i]);
	    	i++;
	    }
	}

    fprintf(file, "}");

	fclose(file);
}

int main(int argc, char** argv){

	authors = NULL;
	int i;
	Authors new;

	init();
	yylex();

	char *name = malloc(100*sizeof(char*));
	
	if(argc >= 2){
		for(i = 1; i < argc-1; i++){
			strcat(name,argv[i]);
			strcat(name," ");
		}
		strcat(name,argv[i]);
		new = getNodeByName(authors,name);
		if(new){
		 printf("Dot file created using author: %s\n",name);
		 createGraph(new);
		}else{
		 printf("Could not create Dot file, author does not exists!\n");
		}
	}else{
		printf("Dot file created using author: Manfred Broy\n");
		name = "Manfred Broy";
		new = getNodeByName(authors,name);
		createGraph(new);
	}

	return 0;
}