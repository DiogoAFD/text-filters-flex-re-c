#ifndef estrutura
#define estrutura

#define ARRAY_SIZE 30


typedef struct coAuthors{
	char **names;
	int size,length;
}* CoAuthors;

typedef struct authorAVL *Authors;

typedef struct authorAVL{
	char *name;
	CoAuthors coAuthors;
	Authors left;
	Authors right;
	int height;
}author_node;


CoAuthors initCoAuthors();
Authors newNode (char *name);
void addCoAuthorToAVL(CoAuthors coAuthors, char *coAuthor);
Authors rightRotate(Authors y);
Authors leftRotate(Authors x);
Authors insert(Authors node, Authors new);
int search(Authors node, char* name);
Authors getNodeByName(Authors node, char *name);
void addAuthors();
int coAuthorExists(char* coAuthor);
void addCoAuthor(char *coAuthor);
int height(Authors A);
int max(int a, int b);
int getBalance(Authors A);
void init();
void createGraph(Authors author);

#endif