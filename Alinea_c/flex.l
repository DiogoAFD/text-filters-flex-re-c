%{
	#include "estrutura.h"
	
	#include <stdio.h>
	#include <string.h>
	

	char authorName[100];
%}

delimiter	[ ]+and[ ]+
authorF		author[ ]+=[ ]+\"
endOfLine	\"\,\n

%x author

%%

{authorF}						{
									BEGIN(author);
								}

<author>{delimiter} 			{
									addCoAuthor(authorName);
									authorName[0] = '\0';
								}

<author>[^ ]+/{endOfLine}		{
									strcat(authorName,strdup(yytext));
									addCoAuthor(authorName);
									authorName[0] = '\0';
									addAuthors();
									init();
									BEGIN(INITIAL);
								}

<author>[^ ]+					{
									strcat(authorName,strdup(yytext));
									strcat(authorName," ");
								}

<author>[^ ]+/{delimiter} 		{
									strcat(authorName,strdup(yytext));
								}

.|\n							;

%%