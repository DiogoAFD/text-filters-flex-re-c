#include "estrutura.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CAT_SIZE 30

Categorias categorias;


//adiciona uma categoria ás categorias, se existir incrementa as ocorrencias
void addCategoria(char *categoria){
	int i,resize;
	if((i = jaExiste(categoria)) != -1){
		categorias->categoria[i]->ocorre++;
	}else{
		if(CAT_SIZE == categorias->totalCat){
			resize = (categorias->tamanho)*2;
			categorias->categoria = realloc(categorias->categoria, resize);
			categorias->tamanho = resize;
			for(i=categorias->totalCat; i < resize; i++) 
				categorias->categoria[i] = NULL;
		}
		Categoria new = (Categoria)malloc(sizeof(Categoria));
		new->nome = strdup(categoria);
		new->ocorre = 1;
		categorias->categoria[categorias->totalCat]=new;
		categorias->totalCat++;
	}
}

//Funcao para alocar espaco
void init(){
	int i;
	categorias = (Categorias)malloc(sizeof(Categorias));
	categorias->categoria=
			(Categoria*)malloc(CAT_SIZE*sizeof(Categoria));

	for(i=0;i<CAT_SIZE;i++) 
		categorias->categoria[i] = NULL;

	categorias->totalCat = 0;
	categorias->tamanho = CAT_SIZE;
}

//Coloca as strings todas em minusculas
void lower_string(char s[]){
   int c = 0;
   while (s[c] != '\0') {
      if (s[c] >= 'A' && s[c] <= 'Z') {
         s[c] = s[c] + 32;
      }
      c++;
   }
}

// verifica se uma dada categoria ja existe
// caso a categoria exista retorna o indice, senão retorna -1
int jaExiste(char *categoria){
	int i=0,j;
	char *categoria1,*categoria2;
	categoria1 = strdup(categoria);
	lower_string(categoria1);
	while(categorias->categoria[i]){
		categoria2 = strdup(categorias->categoria[i]->nome);
		lower_string(categoria2);
		if(strcmp(categoria2,categoria1) == 0){
			return i;
		}
		i++;
	}
	return -1;
}

//Funcao para criar o ficheiro .html
void criaHTML(char *fileName){
	FILE* file;

	Categoria *aux = categorias->categoria;
	
	file=fopen(fileName,"w+");

	fprintf(file, 
	"<!DOCTYPE html>\n<html>\n");
	fprintf(file, 
	"<head>\n <meta charset=\"UTF-8\">\n<title>BibTeX</title>\n</head>\n");

	fprintf(file, 
	"<body>\n<h1>Lista de Categorias e respetiva ocorrência</h1>\n<ul>\n");

	int i;

	while(i < categorias->totalCat){
    	fprintf(file, "<li>%s -> %d\n</li>",
    		categorias->categoria[i]->nome,
    		categorias->categoria[i]->ocorre);
    	i++;
    }
	
	fprintf(file,"</ul>\n");

	fprintf(file, 
		"<p>Total de Categorias encontradas no ficheiro: %d</p>\n", totCat());
	fprintf(file, 
		"</body>\n</html>");

	fclose(file);
}

//total de categorias existentes no ficheiro
int totCat(){
	int i = 0,cont = 0;
	while(categorias->categoria[i]){
		cont += categorias->categoria[i]->ocorre;
		i++;
	}
	return cont;
}

int main(int argc, char* argv[]){
	
	init();
	yylex();
	
	if(argc==2){
		criaHTML(argv[1]);
	}
	else{
		criaHTML("Categorias.html");
	}

	return 0;
}