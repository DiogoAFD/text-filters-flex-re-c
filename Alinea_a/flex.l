%{

	#include <string.h>
	#include "estrutura.h"

	char *categoria;

%}
%%

\@[a-zA-Z]+\{		{
						yytext[strlen(yytext)-1]='\0';
						categoria = (strchr(yytext,'@')+1);
						addCategoria(categoria);
					}

.|\n				;

%%