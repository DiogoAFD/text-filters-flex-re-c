#ifndef estrutura
#define estrutura

typedef struct categoria{
	char *nome;
	int ocorre;
}*Categoria;

typedef struct categorias{
	Categoria *categoria;
	int totalCat,tamanho;
}*Categorias;


void addCategoria(char *categoria);
void init();
void lower_string(char s[]);
int jaExiste(char *categoria);
void criaHTML(char *fileName);
int totCat();

#endif