%{
	#include "estrutura.h"
	
	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>
	

	char *authorName,*titleField = NULL,*text;
	int printAutorL = 0;
	
%}

delimiter		[ ]+and[ ]+
authorF			author[ ]+=[ ]+\"
otherField		[a-zA-Z]+[ ]+=[ ]+[\"]?
endOfLine		\"\,\n
catF			@[a-zA-Z]+\{[a-zA-Z0-9]+\,
titleF			title[ ]+=[ ]+\"

%x categoria
%x autor
%x titulo
%x campo

%%

{catF}							{
									printAutorL = 0;
									printStringNewLine(yytext);
									BEGIN(categoria);
								}

<categoria>{authorF}			{
									authorName = malloc(sizeof(100));
									yytext[strlen(yytext)-1]='{';
									printTabStringNewLine(strdup(yytext));
									BEGIN(autor);
								}

<autor>{delimiter} 				{
									authorName = mudaFormato(authorName);
									strcat(authorName,strdup(yytext));
									print2TabStringNewLine(authorName);
									authorName[0] = '\0';
								}

<autor>[^ ]+/{endOfLine}		{
									strcat(authorName,strdup(yytext));
									authorName = mudaFormato(authorName);
									strcat(authorName,"\n\t\t},\n");
									print2TabString(authorName);
									authorName[0] = '\0';
									printAutorL = 1;
									if(titleField != NULL){
										printTabStringNewLine(titleField);
										titleField = NULL;
									}
									BEGIN(categoria);
								}

<autor>[^ ]+					{
									strcat(authorName,strdup(yytext));
									strcat(authorName," ");
								}

<autor>[^ ]+/{delimiter} 		{
									strcat(authorName,strdup(yytext));
								}

<categoria>{titleF}				{
									yytext[strlen(yytext)-1]='{';
									titleField = strdup(yytext);
									BEGIN(titulo);
								}

<titulo>.*\"\,					{
									yytext[strlen(yytext)-2]='}';
									strcat(titleField,strdup(yytext));
									if(printAutorL == 1){
										printTabStringNewLine(titleField);
										titleField = NULL;
									}
									printAutorL = 0;
									BEGIN(categoria);
								}

<categoria>{otherField}			{	
									yytext[strlen(yytext)-1] = '{';
									text = strdup(yytext);
									addTexto(text);
									free(text);
									BEGIN(campo);
								}

<campo>.*[\"\,]?				{
									if(yytext[strlen(yytext)-1] == ','){
										yytext[strlen(yytext)-2] = '}';
									}else{
										yytext[strlen(yytext)-1] = '}';
									}
									text = strdup(yytext);
									addTexto(text);
									free(text);
									BEGIN(categoria);
								}

<categoria>\}					{
									if(printAutorL == 0 && titleField != NULL){
										printTabStringNewLine(titleField);
										titleField = NULL;
									}
									printCampos();
									printTabStringNewLine(yytext);
									BEGIN(INITIAL);
								}

.|\n							;

%%