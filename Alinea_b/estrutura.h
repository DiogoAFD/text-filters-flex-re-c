#ifndef estrutura
#define estrutura

#define ARRAY_SIZE 100


typedef struct campos{
	char **texto;
	int tamanho,indice,aqui;
}*Campos;


void initCampos();
void addTexto(char *texto);
char *mudaFormato(char *author);
void printCampos();
void printStringNewLine(char *line);
void print2TabStringNewLine(char *line);
void print2TabString(char *line);
void printTabStringNewLine(char *line);
void printTabString(char *line);

#endif