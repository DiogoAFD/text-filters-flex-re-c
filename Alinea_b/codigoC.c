#include "estrutura.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

FILE *file;
char *fileName;

Campos campos;


void initCampos(){
	int i;
	campos = (Campos)malloc(sizeof(Campos));
	campos->texto = (char**)malloc(ARRAY_SIZE*sizeof(char*));
	for(i=0;i<ARRAY_SIZE;i++) campos->texto[i] = NULL;
	campos->indice = 0;
	campos->tamanho = ARRAY_SIZE;
	campos->aqui = 0;
}

void addTexto(char *texto){
	int i,resize;
	if(ARRAY_SIZE == campos->indice){
		resize = (campos->tamanho)*2;
		campos->texto = realloc(campos->texto,resize);
		campos->tamanho = resize;
		for(i=campos->indice; i < resize; i++) campos->texto[i] = NULL;
	}
	campos->texto[campos->indice] = strdup(texto);
	campos->indice++;
}

//Coloca no formato N. Apelido
char *mudaFormato(char *author){
	int i,j=1;
	char *token,*nome,*apelido,*res,*espaco=" ";
	token=strtok(author," ");
	nome = token;
	while(token != NULL){
		apelido = token;
		token = strtok(NULL," ");
	}
	nome[1]='.';
	nome[2]='\0';

	res = strcat(nome,espaco);
	res = strcat(res,apelido);
	
	return res;
}

void printCampos(){
	int i = campos->aqui;
	while(i < campos->indice){
		if(i%2==0){
			printTabString(campos->texto[i]);
		}else{
			printStringNewLine(campos->texto[i]);
		}
		i++;
	}
	campos->aqui = i;
}

void printStringNewLine(char *line){
	fprintf(file,"%s\n", line);
}

void print2TabStringNewLine(char *line){
	fprintf(file,"\t\t%s\n",line);
}

void print2TabString(char *line){
	fprintf(file,"\t\t%s",line);
}

void printTabStringNewLine(char *line){
	fprintf(file,"\t%s\n",line);
}

void printTabString(char *line){
	fprintf(file,"\t%s",line);
}

int main(int argc, char** argv){
	
	initCampos();

	if(argc == 2){
		fileName = argv[1];
		strcat(argv[1],".bib");
	}else{
		fileName = "output.bib";
	}
	file = fopen(fileName,"w+");
	yylex();
	fclose(file);
	return 0;
}